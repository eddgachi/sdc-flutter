void main() {
//   Dart is a statically typed
//   language which means that you
//   have to declare the type of
//   each variable.
  int age = 30; // whole numbers
  double pi = 3.14; // decimal values
  bool isSignedIn = true; // can be true or false
  final String name = "John Doe"; // store text values
  String email;
  bool checkName = name.contains("John");
  email = "[janedoe@mail.com](mailto:janedoe@mail.com)";
//   name = "Anonymous User";
//   print(name);
//   print(checkName);
//   print(age.isEven);
//   print(age.isOdd);
//   print("Hello, World");
//   print("Cart Total: ${5 + 5 - 2 * 80 + 4}");
//   print("your age is " + age.toString());
// collections
  List<String> fruits = ["banana", "apple", "orange"];
  fruits[0] = "mango";
  print(fruits[1]);
  print(fruits[0]);
  fruits.add('banana');
  List user = [23, "Jane Doe", "[janedoe@mail.com](mailto:janedoe@mail.com)"];
  List<dynamic> user1 = [
    23,
    23,
    "Jane Doe",
    "[janedoe@mail.com](mailto:janedoe@mail.com)"
  ];
//   Set<int> numbers = {1, 1, 2, 3, 4};
  Map<String, dynamic> userData = {
    "name": "Jane Doe",
    "age": 23,
    "email": "[janedoe@mail.com](mailto:janedoe@mail.com)",
    "followers": ["ian", "mary", "martin"],
  };
  print(userData["name"]);
  userData["name"] = "Mary Doe";
  userData["bio"] = "Welcome to my page";
//   write a map called prices. the keys are strings
//   and the values are integers
//   data => apple = 100, banana = 200, orange = 300
  Map<String, int> prices = {
    "apple": 100,
    "banana": 200,
    "orange": 300,
  };

  int x = 5;
  x = x + 5;
  x += 5;
  // int y = 10;
  // y = y - 10;
  // y -= 10;
//  +=, -= *=, /= -> unary operators
//   +, -, /, * -> mathematical operators
// >, <, ==, !=, >=, <= -> comparison operators
//   && -> both are correct, || -> one is correct

//   Control flow statements are used to
//     control the execution of a program
//     based on certain conditions
  int userAge = 17;
  String userEmail = "[mary@mail.com](mailto:mary@mail.com)";
  String userPassword = "qwerty123";

//   print(userEmail.contains("@"));
//   print(userPassword.length < 8);
//   print(userAge >= 18);

//   if...else: Used to execute a block of code
//     if a condition is true.

  if (userAge >= 18) {
//     runs if the condition is true
    print("You are 18 or above years");
  } else if (userEmail.contains("@")) {
    print("Your email is valid");
  } else if (userPassword.length < 8) {
    print("Your password length is ok");
  } else {
//     the default statement
//     runs if everything else is false
    print("You must be 18 years or above");
  }

//   given a number 13, store it in a variable
//   called number which is an integer. write an
//   if block to check if the number is between
//   1. 0 and 5
//   2. 6 and 10
//   3. 11 and 15
//   4. 16 and 20
  int number = 13;

  if (number >= 0 && number <= 5) {
    print("number is between 0 and 5");
  } else if (number >= 6 && number <= 10) {
    print("number is between 6 and 10");
  } else if (number >= 11 && number <= 15) {
    print("number is between 11 and 15");
  } else if (number >= 16 && number <= 20) {
    print("number is between 16 and 20");
  } else {
    print("number is greater than 20");
  }

//   switch...case: Used to execute a block of code
//   based on different cases.

  int day = 2;

  switch (day) {
    case 1:
      print("Monday");
      break;
    case 2:
      print("Tuesday");
      break;
    case 3:
      print("Wednesday");
      break;
    case 4:
      print("Thursday");
      break;
    case 5:
      print("Friday");
      break;
    default:
      print("The day is a weekend");
  }

//   for, while, and do...while: Used to execute a
//     block of code repeatedly.

  for (int i = 0; i < 10; i++) {
    print(i);
  }

  int y = 6;
  while (y < 6) {
    print("Condition is true");
    y++; // y = y + 1;
  }
//   bool condition = true;
//   do {
//     print('Hello');
//   } while (condition);

//   void sayHello1() {
//     print("Hello User");
//   }

//   sayHello1();
//   sayHello2("John Doe", "[john@mail.com](mailto:john@mail.com)"); -> positional prams
  sayHello2("[john@mail.com](mailto:john@mail.com)", "John Doe");
}

// top level functions - called anywhere
// nested functions - called inside the function they were created

String globalScopeExample = "";

void sayHello2(String name, String email) {
  String sayHelloEmail =
      "[$email@mail.com](mailto:$email@mail.com)"; // local scope
  print(sayHelloEmail);
  print("hello $name");
}

void namedOptionalParams({int? x, double? y, String? greeting}) {
  namedOptionalParams(y: 5.5, x: 6);
}

void nameRequiredParams({
  required int x,
  required double y,
  required String greeting,
}) {
  nameRequiredParams(x: x, y: y, greeting: greeting);
}

void userData({
  required String name,
  required String email,
  int? age,
}) {
  print("Your name is $name and your email is $email");
  if (age != null) {
    print("Your age is $age");
  }
}

// userData(name: "John Doe", age: 43, email: "[john@mail.com](mailto:john@mail.com)");

addNumbers(x, y) {
  int z = x + y;
  return z;
}

int result = addNumbers(5, 6);

// write a void function called mathematicalOperations. the function
// takes in two numbers (required) and does the following mathematical
// operations printing out the result of each to the console
// 1. sum
// 2. subtraction
// 3. multiplication
// 4. division

void mathematicalOperations({required int x, required int y}) {
  print("The sum of $x and $y is ${x + y}");
  print("The subtraction of $x and $y is ${x - y}");
  print("The multiplication of $x and $y is ${x * y}");
  print("The division of $x and $y is ${x / y}");
}
