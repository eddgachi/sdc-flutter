void main() {

// Classes are used in Dart to define a blueprint for objects. 
// Objects are instances of classes that contain data and behavior.
//   User myUser = User();
//   final myUser2 = User();
//   myUser2.hasLongName();
  Person person = Person(name: "John Doe", age: 43);
  person.sayHello();
  
//   myUser2.name = "Jane Doe";
  
//   myUser.name = "John Doe";
//   myUser.userName = "john";
//   myUser.followers = 0;
//   myUser.following = 1;
//   myUser.followUser("Jane");
}

class User {
    String name = "John Doe"; // field
    String? userName;
    String? bio;
    int? following;
    int? followers;
    
    void followUser(String uniqueUserName){
      print("$name you are now following $uniqueUserName");
    }  
    void unfollowUser() {
      print("You are now following xyz");
    }
    
  bool hasLongName() {
    return name.length > 10;
  }
  
  }

// class User {
//     String name; // field
//     final String userName;
//     final String bio;
//     final int following;
//     final int followers;
  
//   User({
// //     required this.name, // named required params
//     required String firstName,
//     required String lastName,
//     required this.userName, 
//     this.bio = "", // named optional params
//     this.following = 0, 
//     this.followers = 0,
//     }) : name = "$firstName $lastName";
    
//     void followUser(String uniqueUserName){
//       print("$name you are now following $uniqueUserName");
//     }  
//     void unollowUser() {
//       print("You are now following xyz");
//     }
//   }

// write a class called Person with the fields String name 
// and int age
// add a function called sayHello. add a constructor 
// instantiating the name and the age. in your main function
//  create a person variable and pass in a name and age then
//  call the function sayHello

class Person {
  String name;
  int age;
  
  Person({
    required this.name, 
    required this.age,
  });
  
  void sayHello() {
    print("Hello, my name is $name and I am $age years old");
  }
}
  
  
  
  
  
  

