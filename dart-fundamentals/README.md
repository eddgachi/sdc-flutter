# Dart Fundamentals

Welcome to the Dart Fundamentals course! This course is part of a larger Flutter development program, and it will provide you with a solid foundation in the Dart programming language.

Dart is the language used by Flutter for building mobile, web, and desktop applications. It is an object-oriented programming language with syntax that is similar to languages like Java and C#. Dart also features a modern syntax, including support for asynchronous programming, which makes it a great fit for developing modern applications.

This course will cover the basic syntax and features of Dart, including variables, data types, control flow statements, functions, and classes. By the end of the course, you will have a good understanding of how to write clean and efficient code in Dart, and you will be ready to start building Flutter apps.

We will start with an introduction to Dart syntax and programming concepts, and then move on to more advanced topics such as object-oriented programming, generics, and asynchronous programming. Throughout the course, we will provide plenty of code examples and exercises to help you reinforce your understanding of the concepts.

We hope you enjoy the course and find it helpful in your journey to becoming a skilled Flutter developer!

## Getting Started

To get started with Dart, you can use [DartPad](dartpad.dev/?), an online code editor that lets you write and run Dart code directly in your browser. DartPad is a great tool for learning and practicing Dart because it requires no setup and allows you to experiment with Dart code in a sandbox environment.

To use DartPad, follow these steps:

- Go to [dartpad.dev](dartpad.dev/?) in your web browser.
- You should see a default Dart program that prints "Hello, World!" to the console. You can modify this code to experiment with Dart.
- Write your Dart code in the editor on the left side of the screen. You can write Dart code directly in the editor, or you can copy and paste code from this course.
- To run your code, click the "Run" button at the top of the screen. DartPad will compile and execute your code, and display the output in the console on the right side of the screen.
- You can also save and share your DartPad code by clicking the "Share" button at the top of the screen. This will give you a unique URL that you can share with others.

To get started with coding and practicing Dart, try modifying the default "Hello, World!" program to print your own message to the console. You can also try writing your own functions and experimenting with different data types.

Have fun exploring Dart and using DartPad!

## Course Outline

## Module 2: Dart Programming Basics

In this section, we will cover the basics of Dart programming, including variables, data types, control flow statements, functions, and classes.

1. Introduction to Dart

- What is Dart?
- Dart syntax and structure
- Setting up your development environment

2. Variables and Data Types

- Declaring variables
- Primitive data types (int, double, bool, etc.)
- Strings and string interpolation
- Constants and final variables

3. Control Flow Statements

- If/else statements
- Switch statements
- Loops (for, while, do-while)
- Break and continue statements

4. Functions

- Declaring and calling functions
- Function parameters and return types
- Optional and named parameters
- Function expressions

5. Classes and Objects

- Creating classes and objects
- Constructors and named constructors
- Class properties and methods
- Getters and setters

6. Exception Handling

- Try/catch blocks
- Rethrowing exceptions
- Custom exceptions

By the end of this section, you will have a strong foundation in Dart programming and be ready to move on to more advanced topics like asynchronous programming and working with libraries.

### 1. Introduction to Dart

#### What is Dart?

Dart is an object-oriented programming language developed by Google. It was designed to be a scalable, flexible, and performant language that could be used to build modern applications for the web, mobile, and desktop platforms.

Dart can be used for a wide range of applications, from small scripts and command-line tools to complex, enterprise-level applications. One of the main strengths of Dart is its support for asynchronous programming, which makes it well-suited for building responsive, event-driven applications.

Dart is used extensively in the development of Flutter, a popular mobile app development framework also developed by Google.

#### Dart syntax and structure

Dart has a syntax that is similar to other popular programming languages such as Java, C++, and C#. Here are some of the key syntax features of Dart:

- ##### Variables

In Dart, variables are declared using the var keyword. The type of the variable is inferred from the assigned value.

```dart
var message = "Hello, world!";
var number = 42;
var isRaining = true;
```

- ##### Functions

Functions in Dart are declared using the function keyword. You can define optional and named parameters for functions.

```dart
// A basic function
void sayHello() {
    print("Hello, world!");
}

// A function with parameters
void greet(String name) {
    print("Hello, $name!");
}

// A function with optional and named parameters
void buyFood(String name, {int quantity = 1, String type}) {
    print("Buying $quantity $type of $name.");
}
```

- ##### Control flow statements

Dart supports common control flow statements such as if/else statements, for loops, and while loops.

```dart
if (isRaining) {
    print("Take an umbrella.");
} else {
    print("Enjoy the sunshine!");
}

for (var i = 0; i < 10; i++) {
    print(i);
}

var i = 0;
while (i < 10) {
    print(i);
    i++;
}
```

- ##### Classes and objects

Dart is an object-oriented language, and classes and objects are a core part of the language.

```dart
class Person {
    String name;
    int age;

    Person(this.name, this.age);

    void sayHello() {
        print("Hello, my name is $name.");
    }
}

var person = Person("Alice", 30);
person.sayHello();
```

- ##### Asynchronous programming

Dart has strong support for asynchronous programming using the async and await keywords. Asynchronous programming is important for building responsive and efficient applications.

```dart
void fetchUserData() async {
    var response = await http.get("https://api.example.com/user");
    var data = jsonDecode(response.body);
    print("User data: $data");
}
```

#### Setting up your development environment

To start coding in Dart, you will need to set up your development environment. Here are the steps to get started:

- Download and install the Dart SDK.
- Install an IDE or code editor that supports Dart, such as **Visual Studio Code** or **IntelliJ IDEA**.
- Create a new Dart project in your IDE or code editor.
- Write your Dart code in the editor, and run it using the Dart command-line tool or your IDE's built

### 2. Variables and Data Types

#### Declaring Variables

In Dart, you can declare variables using the **var** keyword, followed by the variable name and an optional type annotation. For example:

```dart
var myVariable = 42;
int myInt = 42;
String myString = 'Hello, World!';
```

In the first line, the type of the **myVariable** variable is inferred to be **int** based on the initial value of **42**. In the second line, the type of the **myInt** variable is explicitly declared as **int**. In the third line, the type of the **myString** variable is explicitly declared as **String**.

You can also declare multiple variables of the same type on the same line, separated by commas:

```dart
int x = 1, y = 2, z = 3;
```

#### Primitive Data Types

Dart has several primitive data types, including:

- **int** for integers
- **double** for floating-point numbers
- **bool** for boolean values (**true** or **false**)
- **String** for strings of text

Here are some examples of declaring variables of these types:

```dart
int myInt = 42;
double myDouble = 3.14;
bool myBool = true;
String myString = 'Hello, World!';
```

#### Strings and String Interpolation

Strings in Dart are enclosed in either single or double quotes. You can use string interpolation to embed variables or expressions inside a string, by enclosing them in curly braces **{}**. For example:

```dart
String name = 'Alice';
int age = 30;
String message = 'My name is $name and I am $age years old.';
print(message); // Output: My name is Alice and I am 30 years old.
```

In this example, the variables **name** and **age** are embedded inside the string using string interpolation.

#### Constants and Final Variables

In addition to regular variables, Dart also has two types of variables that cannot be reassigned once they are initialized: **final** and **const**.

A **final** variable is a variable that can be assigned a value only once. After it is assigned a value, it cannot be changed. For example:

```dart
final int x = 42;
```

A **const** variable is similar to a **final** variable, but it is a compile-time constant. That is, it is a value that is known at compile time and cannot be changed at runtime. For example:

```dart
const double pi = 3.14;
```

Both **final** and **const** variables must be initialized at the time they are declared.

```dart
void main() {
    // Example of declaring and using variables
    var name = 'Alice';
    int age = 30;

    // Example of string interpolation
    String message = 'My name is $name and I am $age years old.';
    print(message); // Output: My name is Alice and I am 30 years old.

    // Example of using final and const variables
    final int x = 42;
    const double pi = 3.14;
    print(x); // Output: 42
    print(pi); // Output: 3.14
}
```

### 3. Control Flow Statements

Control flow statements are used to control the order in which statements are executed in a program. In Dart, there are several types of control flow statements, including if/else statements, switch statements, loops, break and continue statements.

#### If/Else Statements

If/else statements allow you to execute different blocks of code based on whether a condition is true or false. The basic syntax of an if/else statement is as follows:

```dart
if (condition) {
  // code to execute if condition is true
} else {
  // code to execute if condition is false
}
```

Here's an example:

```dart
int x = 5;

if (x < 10) {
  print("x is less than 10");
} else {
  print("x is greater than or equal to 10");
}
```

In this example, the if statement checks if x is less than 10. Since x is 5, the condition is true, and the first block of code is executed, which prints "x is less than 10" to the console.

#### Switch Statements

Switch statements allow you to execute different blocks of code based on the value of a variable. The basic syntax of a switch statement is as follows:

```dart
switch (variable) {
  case value1:
    // code to execute if variable equals value1
    break;
  case value2:
    // code to execute if variable equals value2
    break;
  // more cases here
  default:
    // code to execute if none of the cases match
    break;
}
```

Here's an example:

```dart
String color = "red";

switch (color) {
  case "red":
    print("The color is red");
    break;
  case "blue":
    print("The color is blue");
    break;
  case "green":
    print("The color is green");
    break;
  default:
    print("Unknown color");
    break;
}
```

In this example, the switch statement checks the value of the variable **color**. Since **color** is **"red"**, the first block of code is executed, which prints "The color is red" to the console.

#### Loops

Loops allow you to repeat blocks of code a certain number of times or until a certain condition is met. There are three types of loops in Dart: **for** loops, **while** loops, and **do-while** loops.

##### 1. For Loops

**for** loops allow you to execute a block of code a certain number of times. The basic syntax of a **for** loop is as follows:

```dart
for (initialization; condition; increment/decrement) {
  // code to execute in the loop
}
```

Here's an example:

```dart
for (int i = 0; i < 5; i++) {
  print("The value of i is $i");
}
```

In this example, the **for** loop initializes the variable **i** to **0**, checks if **i** is less than **5**, executes the block of code, increments **i** by **1**, and repeats the process until **i** is no longer less than **5**. The output of this code is:

```bash
The value of i is 0
The value of i is 1
The value of i is 2
The value of i is 3
The value of i is 4
```

##### 2. While Loops

**while** loops allow you to execute a block of code while a condition is true. The basic syntax of a while loop is as follows:

```dart
while (condition) {
  // code to execute in the loop
}
```

Here's an example:

```dart
int i = 0;

while (i < 5) {
  print("The value of i is $i");
  i++;
}
```

In this example, the **while** loop checks if **i** is less than **5**, executes the block of code, increments **i** by **1**, and repeats the process until **i** is no longer less than **5**. The output of this code is:

```bash
The value of i is 0
The value of i is 1
The value of i is 2
The value of i is 3
The value of i is 4
```

##### 3. Do-While Loops

**do-while** loops are similar to **while** loops, but they always execute the block of code at least once. The basic syntax of a **do-while** loop is as follows:

```dart
do {
    // code to execute in the loop
} while (condition);
```

Here's an example:

```dart
int i = 0;

do {
    print("The value of i is $i");
    i++;
} while (i < 5);
```

In this example, the **do-while** loop executes the block of code, increments **i** by **1**, and checks if **i** is less than **5**. The loop repeats the process until **i** is no longer less than **5**. The output of this code is:

```bash
The value of i is 0
The value of i is 1
The value of i is 2
The value of i is 3
The value of i is 4
```

#### Break and Continue Statements

**break** and **continue** statements allow you to control the flow of execution within loops.

**break** is used to exit a loop early. When **break** is encountered inside a loop, the loop is immediately terminated, and the program continues executing after the loop. Here's an example:

```dart
for (int i = 0; i < 5; i++) {
    if (i == 3) {
    break;
}
    print("The value of i is $i");
}
```

In this example, the loop terminates when **i** is equal to **3**. The output of this code is:

```bash
The value of i is 0
The value of i is 1
The value of i is 2
```

**continue** is used to skip the current iteration of a loop and continue with the next iteration. When **continue** is encountered inside a loop, the current iteration is skipped, and the loop continues with the next iteration. Here's an example:

```dart
for (int i = 0; i < 5; i++) {
if (i == 3) {
continue;
}
print("The value of i is $i");
}
```

In this example, the loop skips the iteration when **i** is equal to **3**. The output of this code is:

```bash
The value of i is 0
The value of i is 1
The value of i is 2
The value of i is 4
```

### 4. Functions

##### Declaring and Calling Functions

A function is a block of code that performs a specific task. In Dart, you can declare a function using the **function** keyword, followed by the function name and a set of parentheses. The code that makes up the function is enclosed in curly braces **{}**. Here's an example of a simple function:

```dart
void sayHello() {
print('Hello, world!');
}
```

This function, named **sayHello()**, simply prints the string "Hello, world!" to the console when it's called. The **void** keyword in front of the function name indicates that this function doesn't return a value.

To call a function, you simply use its name followed by a set of parentheses. Here's an example of how to call the **sayHello()** function:

```dart
sayHello(); // prints "Hello, world!" to the console
```

##### Function Parameters and Return Types

Functions can also accept input parameters and return output values. In Dart, you can specify the input parameters and return type of a function using the function's signature. Here's an example of a function that accepts two **int** parameters and returns their sum:

```dart
int addNumbers(int a, int b) {
return a + b;
}
```

In this example, the **addNumbers()** function takes two int parameters named a and b, and returns their sum as an int. To call this function and use its return value, you can assign the result to a variable:

```dart
int result = addNumbers(3, 5);
print(result); // prints "8" to the console
```

##### Optional and Named Parameters

In addition to required parameters, Dart also supports optional parameters. Optional parameters can be specified using square brackets **[]** around the parameter name in the function signature. Here's an example of a function with an optional parameter:

```dart
void printMessage(String message, [String name]) {
    if (name != null) {
        print('$message, $name!');
    } else {
        print(message);
    }
}
```

In this example, the **printMessage()** function takes a required parameter **message** of type **String**, and an optional parameter **name** of type **String**. If **name** is not provided, the function simply prints the **message**. If **name** is provided, the function prints the **message** followed by the **name**. Here's how you can call this function:

```dart
printMessage('Hello'); // prints "Hello" to the console
printMessage('Hi', 'Alice'); // prints "Hi, Alice!" to the console
```

Dart also supports named parameters, which can be useful when a function has many optional parameters. Named parameters can be specified using curly braces **{}** around the parameter name in the function signature. Here's an example of a function with named parameters:

```dart
void printName({String firstName, String lastName}) {
print('$firstName $lastName');
}
```

In this example, the **printName()** function takes two named parameters: **firstName** and **lastName**, both of type **String**. Here's how you can call this function:

```dart
printName(firstName: 'Alice', lastName: 'Smith'); // prints "Alice Smith" to the console
```

##### Function Expressions

Dart also supports function expressions, which are a shorthand way of defining functions. Function expressions can be used when a function is only needed in one place, such as when passing a function as a parameter to another function.

Here's an example of a function expression that takes two **int** parameters and returns their sum:

```dart
var addNumbers = (int a, int b) => a + b;
```

In this example, we define a variable named **addNumbers** and assign it a function expression that takes two **int** parameters **a** and **b**, and returns their sum. The **=>** symbol is shorthand for the **return** keyword. To call this function, you can simply use the variable name followed by a set of parentheses:

```dart
int result = addNumbers(3, 5);
print(result); // prints "8" to the console
```

Function expressions can also be used with optional and named parameters, like this:

```dart
var printMessage = (String message, {String name}) {
  if (name != null) {
    print('$message, $name!');
  } else {
    print(message);
  }
};
```

In this example, we define a variable named **printMessage** and assign it a function expression that takes a required parameter **message** of type **String**, and an optional named parameter **name** of type **String**. The function works exactly like the **printMessage()** function we defined earlier.

Function expressions are a powerful tool for writing concise and expressive code. They can be used in many situations where you need to pass a function as a parameter, or define a function inline.

### 5. Classes and Objects

In Dart, a class is a blueprint for creating objects. An object is an instance of a class. Classes define the properties and methods that objects of the class will have.

To create a class in Dart, you use the **class** keyword, followed by the name of the class, and then the class body, which is enclosed in curly braces. Here's an example:

```dart
class Person {
    String name;
    int age;
}
```

In this example, we have created a **Person** class with two properties: **name** and **age**. These properties are not initialized, so their initial values will be **null** and **0**, respectively.

To create an object of a class in Dart, you use the **new** keyword, followed by the name of the class and parentheses. Here's an example:

```dart
Person person = new Person();
```

In this example, we have created a **Person** object called **person**.

#### Constructors and Named Constructors

Constructors are special methods in a class that are used to create objects of the class. In Dart, a constructor has the same name as the class and can take parameters. Here's an example of a constructor in the **Person** class:

```dart
class Person {
    String name;
    int age;

    Person(String name, int age) {
    this.name = name;
    this.age = age;
    }
}
```

In this example, we have added a constructor to the **Person** class that takes two parameters: **name** and **age**. Inside the constructor, we set the **name** and **age** properties of the object to the values of the parameters.

Dart also supports named constructors, which are constructors that have a name other than the class name. Named constructors are useful when you want to create objects of the class in a specific way. Here's an example of a named constructor in the Person class:

```dart
class Person {
    String name;
    int age;

    Person.fromJson(Map<String, dynamic> json) {
    this.name = json['name'];
    this.age = json['age'];
    }
}
```

In this example, we have added a named constructor to the **Person** class called **fromJson**. This constructor takes a **Map** object with **String** keys and **dynamic** values, which represents a JSON object. Inside the constructor, we set the **name** and **age** properties of the object to the values in the JSON object.

#### Class Properties and Methods

Class properties are variables that belong to a class. They define the state of the objects of the class. Here's an example of a **Person** class with some properties:

```dart
class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    String getFullName() {
        return this.name;
    }

    void sayHello() {
        print('Hello, my name is $name.');
    }
}
```

In this example, we have added two methods to the **Person** class: **getFullName** and **sayHello**. **getFullName** is a method that returns the **name** property of the object. **sayHello** is a method that prints a message to the console with the object's name.

#### Getters and Setters

Getters and setters are special methods that allow you to access and modify the properties of an object. Getters are used to get the value of a property, while setters are used to set the value of a property.

In Dart, you can define getters and setters using the **get** and **set** keywords, respectively. Here's an example:

```dart
class Person {
    String \_name;
    int \_age;

    Person(String name, int age) {
        this.\_name = name;
        this.\_age = age;
    }

    String get name {
        return this.\_name;
    }

    set name(String name) {
        this.\_name = name;
    }

    int get age {
        return this.\_age;
    }

    set age(int age) {
        this._age = age;
    }
}
```

In this example, we have added getters and setters for the **name** and **age** properties of the **Person** class. Notice that the properties are prefixed with an underscore (**\_**). This is a convention in Dart to indicate that the property should not be accessed directly from outside the class.

To use the getters and setters, you can access them just like you would access a property. Here's an example:

```dart
Person person = new Person('Alice', 30);
print(person.name); // output: Alice
person.name = 'Bob';
print(person.name); // output: Bob
```

In this example, we create a **Person** object called **person** with the name "Alice" and age 30. We then print the value of the **name** property using the **name** getter. We then use the **name** setter to set the value of the **name** property to "Bob", and then print the value of the **name** property again using the **name** getter.

That's a brief explanation of classes and objects, constructors and named constructors, class properties and methods, and getters and setters in Dart. These are the fundamentals of object-oriented programming in Dart and are essential for understanding how to use Flutter to build mobile applications.

### 6. Exception Handling

In Dart, an exception is a runtime error that occurs when your program encounters an unexpected situation, such as dividing by zero or accessing an index that is out of range. When an exception occurs, it can cause your program to crash if it is not handled properly. To prevent this from happening, Dart provides a mechanism for handling exceptions through try/catch blocks.

#### Try/Catch Blocks

A try/catch block is a structure that allows you to handle exceptions in a controlled manner. The basic syntax of a try/catch block is as follows:

```dart
try {
    // code that might throw an exception
    } catch (e) {
    // code to handle the exception
}
```

The **try** block contains the code that might throw an exception, and the **catch** block contains the code that handles the exception. When an exception is thrown in the try block, control is transferred to the **catch** block, and the **catch** block executes.

In the **catch** block, you can access information about the exception through the e parameter, which is an instance of the **Exception** class. For example, you can print a message that describes the exception:

```dart
try {
// code that might throw an exception
} catch (e) {
print('An exception occurred: $e');
}
```

#### Rethrowing Exceptions

Sometimes, you may want to catch an exception in one part of your code and rethrow it in another part of your code. You can do this using the rethrow keyword, which allows you to **rethrow** the original exception:

```dart
try {
    // code that might throw an exception
} catch (e) {
    // do some processing
    rethrow; // rethrow the exception
}
```

In this example, the **catch** block does some processing on the exception, and then rethrows it using the **rethrow** keyword. This allows the exception to be caught by another **try** block further up the call stack.

#### Custom Exceptions

In addition to the built-in exceptions provided by Dart, you can also define your own custom exceptions. To define a custom exception, you need to create a new class that extends the **Exception** class:

```dart
class MyException implements Exception {
    final String message;

    MyException(this.message);

    @override
    String toString() => 'MyException: $message';
}
```

In this example, **MyException** is a custom exception that takes a **message** parameter in its constructor. The **toString** method is overridden to provide a string representation of the exception.

You can throw a custom exception using the **throw** keyword:

```dart
void myFunction() {
    throw MyException('Something went wrong');
}
```

In this example, the **myFunction** function throws a **MyException** with the message "Something went wrong". The exception can be caught using a **try/catch** block:

```dart
try {
    myFunction();
} catch (e) {
    print('An exception occurred: $e');
}
```

In this example, the **catch** block handles the **MyException** thrown by **myFunction**.
