class ChatUser {
  final String uid;
  final String name;
  final String email;
  final String username;
  final String bio;

  ChatUser({
    required this.uid,
    required this.name,
    required this.email,
    required this.username,
    required this.bio,
  });
}
