import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat_app/models/chat_user.dart';

class AuthenticationService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  // Helper method to create a ChatUser object from Firebase User
  ChatUser _userFromFirebase(User user) {
    return ChatUser(
      uid: user.uid,
      // These values will be fetched from Firestore
      name: '',
      email: '',
      username: '',
      bio: '',
    );
  }

  Future<ChatUser?> fetchUserAndUpdateUserDetails(ChatUser chatUser) async {
    DocumentSnapshot userSnapshot =
        await _firestore.collection('users').doc(chatUser.uid).get();

    if (userSnapshot.exists) {
      Map<String, dynamic> userData =
          userSnapshot.data()! as Map<String, dynamic>;
      ChatUser updatedChatUser = ChatUser(
        uid: chatUser.uid,
        name: chatUser.name,
        email: chatUser.email,
        username: chatUser.username,
        bio: chatUser.bio,
      );
      return updatedChatUser;
    } else {
      return null;
    }
  }

  Future<ChatUser?> signInWithEmailAndPassword(
    String email,
    String password,
  ) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      ChatUser chatUser = _userFromFirebase(userCredential.user!);
      return fetchUserAndUpdateUserDetails(chatUser);
    } on FirebaseAuthException catch (e) {
      print('Error: $e');
      return null;
    } catch (e) {
      print('Error: $e');
      return null;
    }
  }
}
