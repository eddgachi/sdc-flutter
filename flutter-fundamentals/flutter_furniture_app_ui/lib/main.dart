import 'package:flutter/material.dart';
import 'package:flutter_furniture_app_ui/screens/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Furniture App U.I',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // yellow-orange = #f2a313
        // chair card background = #f5f7fb
        // cart minus = #a8a8a8
        // cart plus = #5f6088
        // details color 1 = #5f6088
        // details color 2 = #e44546
        // details color 3 = #4340b3
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
