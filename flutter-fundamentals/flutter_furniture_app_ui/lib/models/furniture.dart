class Furniture {
  String title;
  String imageUrl;
  String description;
  double price;
  Furniture({
    required this.title,
    required this.imageUrl,
    required this.description,
    required this.price,
  });
}
