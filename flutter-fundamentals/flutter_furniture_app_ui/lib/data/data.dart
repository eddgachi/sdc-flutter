import 'package:flutter_furniture_app_ui/models/furniture.dart';

String _description =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lacus lorem, gravida eget purus a, sagittis feugiat arcu. Etiam iaculis feugiat porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis posuere condimentum pellentesque. Cras condimentum metus sed nisi mattis, non elementum velit fringilla. Nam tellus dui, pretium ultricies congue vel, cursus et urna. Nam finibus bibendum dolor in maximus. Nulla consectetur lacus justo, id volutpat ante pellentesque ut. Suspendisse a finibus turpis.';

Furniture furniture_1 = Furniture(
  title: 'Chair One',
  imageUrl: 'assets/img_1.png',
  description: _description,
  price: 34.99,
);

Furniture furniture_2 = Furniture(
  title: 'Chair Two',
  imageUrl: 'assets/img_2.png',
  description: _description,
  price: 34.99,
);

Furniture furniture_3 = Furniture(
  title: 'Chair Three',
  imageUrl: 'assets/img_3.png',
  description: _description,
  price: 34.99,
);

Furniture furniture_4 = Furniture(
  title: 'Chair Four',
  imageUrl: 'assets/img_4.png',
  description: _description,
  price: 34.99,
);
