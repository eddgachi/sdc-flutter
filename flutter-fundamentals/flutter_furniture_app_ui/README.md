# Furniture Shop App

This is a Flutter application that replicates the user interface of a furniture shop app, based on a design found on [dribbble.com](https://dribbble.com). The purpose of this application is to help you understand the basics of Flutter, building custom components, working with third-party packages, and how to build a real-world user interface.

## Design

The design we are replicating is called "Furniture Shop App" and was created by [Rakib Kowshar](https://dribbble.com/rakibkowshar). It features a simple and modern design, with a focus on showcasing furnitures and making it easy to shop for them.

## Design Screenshot

![design screenshot](https://cdn.dribbble.com/users/1720295/screenshots/15363480/media/f8954344658dcc7d2f636695f77b8d75.png?compress=1&resize=1000x750&vertical=top)

## Features

This furniture shop app replicates the following user interface features:

- A welcome screen with a logo, greeting message, and a button to proceed to the home screen.
- A home screen with a search bar, a list of furniture categories, and a list of recommended furnitures.
- A furniture details screen with more information about a selected furniture, including its image, name, price, and description, as well as a button to add it to the cart.
- A cart screen with a list of added furnitures, their quantities, and the total price, as well as buttons to remove furnitures or proceed to checkout.
- A checkout screen with a form for entering shipping information, a list of ordered furnitures, and the total price, as well as a button to confirm the order.

## Technologies Used

This furniture shop app was developed using Flutter, an open-source framework for building high-performance, high-fidelity mobile applications for iOS and Android, using a single codebase.

The app uses basic Flutter widgets to replicate the design, including:

- **Scaffold** widget for setting up the app's basic layout.

## License

This digital banking app is released under the MIT License. See the LICENSE file for more information.

## Credits

The design for this digital banking app was created by [Rakib Kowshar](https://dribbble.com/rakibkowshar) and is used with permission.
