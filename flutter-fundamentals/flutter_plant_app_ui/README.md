# Plant Shop App

This is a Flutter application that replicates the user interface of a plant shop app, based on a design found on [dribbble.com](https://dribbble.com). The purpose of this application is to help you understand the basics of Flutter, building custom components, working with third-party packages, and how to build a real-world user interface.

## Design

The design we are replicating is called "Plant Shop App" and was created by [Rakib Kowshar](https://dribbble.com/rakibkowshar). It features a simple and modern design, with a focus on showcasing plants and making it easy to shop for them.

## Design Screenshot

![design screenshot](https://cdn.dribbble.com/userupload/3363270/file/original-348dbc230d2256fd642f503aa0c57269.jpg?compress=1&resize=752x)

## Features

This plant shop app replicates the following user interface features:

- A welcome screen with a logo, greeting message, and a button to proceed to the home screen.
- A home screen with a search bar, a list of plant categories, and a list of recommended plants.
- A plant details screen with more information about a selected plant, including its image, name, price, and description, as well as a button to add it to the cart.
- A cart screen with a list of added plants, their quantities, and the total price, as well as buttons to remove plants or proceed to checkout.
- A checkout screen with a form for entering shipping information, a list of ordered plants, and the total price, as well as a button to confirm the order.

## Technologies Used

This plant shop app was developed using Flutter, an open-source framework for building high-performance, high-fidelity mobile applications for iOS and Android, using a single codebase.

The app uses basic Flutter widgets to replicate the design, including:

- **Scaffold** widget for setting up the app's basic layout.

## License

This digital banking app is released under the MIT License. See the LICENSE file for more information.

## Credits

The design for this digital banking app was created by [Rakib Kowshar](https://dribbble.com/rakibkowshar) and is used with permission.
