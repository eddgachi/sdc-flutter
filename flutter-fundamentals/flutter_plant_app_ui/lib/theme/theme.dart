import 'package:flutter/material.dart';

TextStyle welcomeHeadingTextStyle = TextStyle(
  fontSize: 38.0,
  fontWeight: FontWeight.bold,
);

TextStyle normalTextStyle = TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.bold,
);

TextStyle normalTextStyleTwo = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.bold,
);

TextStyle normalTextStyleThree = TextStyle(
  color: Colors.grey,
  fontSize: 14.0,
  fontWeight: FontWeight.bold,
);
