import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/constants/constants.dart';
import 'package:flutter_plant_app_ui/data/data.dart';
import 'package:flutter_plant_app_ui/models/plants.dart';
import 'package:flutter_plant_app_ui/screens/plant_details_screen.dart';
import 'package:flutter_plant_app_ui/theme/theme.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
          top: 20.0,
          left: 15.0,
          bottom: 15.0,
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Find your \nfavourite plants',
                    style: welcomeHeadingTextStyle.copyWith(
                      fontSize: 26.0,
                    ),
                  ),
                  Container(
                    width: 50.0,
                    height: 65.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Color(0xFFAEAEB0).withOpacity(0.2),
                        width: 5.0,
                        style: BorderStyle.solid,
                      ),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Center(
                      child: Icon(
                        EvaIcons.search,
                        size: 28.0,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40.0, right: 20.0),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                    color: kDiscountColor,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 50.0, left: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '30% OFF',
                            style: welcomeHeadingTextStyle.copyWith(
                              fontSize: 28.0,
                            ),
                          ),
                          Text(
                            '02 - 23 July',
                            style: normalTextStyle.copyWith(
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Image.asset(
                      'assets/img_8.png',
                      width: 200.0,
                      height: 150.0,
                    ),
                  ],
                )
              ],
            ),
            //  TODO: implement tab widget
            SizedBox(height: 20.0),
            // ListView.builder()
            // scrollDirection - horizontal or vertical
            // itemCount - number of loops
            // itemBuilder - return the u.i to be displayed multiple times
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.45,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: plants.length,
                itemBuilder: (BuildContext context, int index) {
                  // Plant my_plant = plants[0];
                  Plant plant = plants[index];
                  return GestureDetector(
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => PlantDetailsScreen(singlePlant: plant),
                      ),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(top: 20.0, right: 20.0),
                      padding: EdgeInsets.all(10.0),
                      width: MediaQuery.of(context).size.width * 0.5,
                      decoration: BoxDecoration(
                        color: kPlantCardBackgroundColor,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              '\$${plant.price}',
                              style: normalTextStyle.copyWith(fontSize: 14.0),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: RotatedBox(
                              quarterTurns: -1,
                              child: Text(
                                plant.title,
                                style: normalTextStyle.copyWith(fontSize: 14.0),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Image.asset(
                                plant.imageUrl,
                                height: 250.0,
                                width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    height: 30.0,
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 10.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Add to Cart',
                                        style: normalTextStyle.copyWith(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 30.0,
                                    width: 30.0,
                                    decoration: BoxDecoration(
                                      color: Colors.black,
                                      borderRadius: BorderRadius.circular(50.0),
                                    ),
                                    child: Icon(
                                      EvaIcons.heartOutline,
                                      color: Colors.white,
                                      size: 15.0,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
