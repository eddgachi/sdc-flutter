import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/constants/constants.dart';
import 'package:flutter_plant_app_ui/models/plants.dart';
import 'package:flutter_plant_app_ui/theme/theme.dart';

class PlantDetailsScreen extends StatelessWidget {
  Plant singlePlant;
  PlantDetailsScreen({super.key, required this.singlePlant});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Details',
          style: normalTextStyleTwo.copyWith(
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [],
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: [
            Image.asset(
              '${singlePlant.imageUrl}',
              height: MediaQuery.of(context).size.height * 0.48,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  singlePlant.title,
                  style: normalTextStyleTwo,
                ),
                Row(
                  children: [
                    Icon(EvaIcons.star, size: 15.0, color: kPrimaryColor),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: '${singlePlant.rating}',
                            style: normalTextStyleTwo.copyWith(
                              color: Colors.black,
                            ),
                          ),
                          TextSpan(
                            text: ' (268 Reviews)',
                            style: normalTextStyleThree,
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
