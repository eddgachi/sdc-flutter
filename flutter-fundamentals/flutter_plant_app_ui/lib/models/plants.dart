class Plant {
  final String title;
  final String imageUrl;
  final String description;
  final double rating;
  final String size;
  final String plantType;
  final double height;
  final int humidity;
  final double price;
  Plant({
    required this.title,
    required this.imageUrl,
    required this.description,
    required this.rating,
    required this.size,
    required this.plantType,
    required this.height,
    required this.humidity,
    required this.price,
  });
}
