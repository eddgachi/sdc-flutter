import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/constants/constants.dart';
import 'package:flutter_plant_app_ui/screens/welcome_screen.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Plant Shop UI',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        // green - #51ad9b
        // discount card - #d2eac2
        // black used - #050505
        // plant card background - #e7e7e7
        textTheme: GoogleFonts.poppinsTextTheme(
          Theme.of(context).textTheme,
        ),
        scaffoldBackgroundColor: kScaffoldBackgroundColor,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: kPrimaryColor,
        ),
      ),
      home: WelcomeScreen(),
    );
  }
}
