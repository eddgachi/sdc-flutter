import 'package:flutter_plant_app_ui/models/plants.dart';

String _description =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lacus lorem, gravida eget purus a, sagittis feugiat arcu. Etiam iaculis feugiat porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis posuere condimentum pellentesque. Cras condimentum metus sed nisi mattis, non elementum velit fringilla. Nam tellus dui, pretium ultricies congue vel, cursus et urna. Nam finibus bibendum dolor in maximus. Nulla consectetur lacus justo, id volutpat ante pellentesque ut. Suspendisse a finibus turpis.";

final plant_1 = Plant(
  title: "Emberfrond",
  imageUrl: "assets/img_1.png",
  description: _description,
  rating: 4.3,
  size: "Medium",
  plantType: "Fern",
  height: 12.6,
  humidity: 82,
  price: 34.99,
);

final plant_2 = Plant(
  title: "Frostleaf",
  imageUrl: "assets/img_2.png",
  description: _description,
  rating: 4.8,
  size: "Medium",
  plantType: "Perennial",
  height: 12.6,
  humidity: 82,
  price: 41.99,
);

final plant_3 = Plant(
  title: "Moonpetal",
  imageUrl: "assets/img_3.png",
  description: _description,
  rating: 4.3,
  size: "Medium",
  plantType: "Annual",
  height: 12.6,
  humidity: 82,
  price: 23.99,
);

final plant_4 = Plant(
  title: "Fireburst",
  imageUrl: "assets/img_4.png",
  description: _description,
  rating: 4.3,
  size: "Medium",
  plantType: "Annual",
  height: 12.6,
  humidity: 82,
  price: 34.99,
);

final plant_5 = Plant(
  title: "Misty Mountain",
  imageUrl: "assets/img_5.png",
  description: _description,
  rating: 4.8,
  size: "Medium",
  plantType: "Perennial",
  height: 12.6,
  humidity: 82,
  price: 41.99,
);

final plant_6 = Plant(
  title: "Sunset Vine",
  imageUrl: "assets/img_6.png",
  description: _description,
  rating: 4.3,
  size: "Medium",
  plantType: "Vine",
  height: 12.6,
  humidity: 82,
  price: 23.99,
);

final plant_7 = Plant(
  title: "Silverleaf Sage",
  imageUrl: "assets/img_7.png",
  description: _description,
  rating: 4.3,
  size: "Medium",
  plantType: "Herb",
  height: 12.6,
  humidity: 82,
  price: 34.99,
);

final plant_8 = Plant(
  title: "Ocean Spray",
  imageUrl: "assets/img_8.png",
  description: _description,
  rating: 4.8,
  size: "Medium",
  plantType: "Shrub",
  height: 12.6,
  humidity: 82,
  price: 41.99,
);

final List<Plant> plants = [
  plant_1,
  plant_2,
  plant_3,
  plant_4,
  plant_5,
  plant_6,
];

final List<Plant> cartItems = [
  plant_2,
  plant_3,
];
