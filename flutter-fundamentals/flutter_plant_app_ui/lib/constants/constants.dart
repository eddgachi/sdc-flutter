import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF51AD9B);
const kScaffoldBackgroundColor = Colors.white;
const kDiscountColor = Color(0xFFD2EAC2);
const kBlackColor = Color(0xFF050505);
const kPlantCardBackgroundColor = Color(0xFFE7E7E7);
