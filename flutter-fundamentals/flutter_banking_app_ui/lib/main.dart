import 'package:flutter/material.dart';
import 'package:flutter_banking_app_ui/screens/welcome_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Banking App U.I',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        // color=#3213F5
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: Color(0xFF3213F5),
        ),
        primaryColor: Color(0xFF3213F5),
      ),
      home: WelcomeScreen(),
    );
  }
}
