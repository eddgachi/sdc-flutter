# Digital Banking App

This is a Flutter application that replicates the user interface of a digital banking app, based on a design found on [dribbble.com](https://dribbble.com). The purpose of this application is to help you understand the basics of Flutter and how to build a real-world user interface.

## Design

The design we are replicating is called "Digital Banking App" and was created by [Farrel Putra](https://dribbble.com/farrelputra). It features a simple and modern design, with a focus on usability and functionality.

## Design Screenshot

![design screenshot](https://cdn.dribbble.com/users/2744859/screenshots/15097534/media/b832dc01e201fbd57fe474c520d3e145.jpg)

# Features

This digital banking app replicates the following user interface features:

- A welcome screen with an image sourced from undraw.co.
- A dashboard screen with a balance overview and transaction history.

## Technologies Used

This digital banking app was developed using Flutter, an open-source framework for building high-performance, high-fidelity mobile applications for iOS and Android, using a single codebase.

The app uses basic Flutter widgets to replicate the design, including:

- **Scaffold** widget for setting up the app's basic layout.
- **Column** and **Row** widgets for organizing UI elements vertically and horizontally.
- **Container** widget for creating reusable UI components with styling.
- **Padding** widget for adding padding around UI elements.
- **SizedBox** widget for adding empty space between UI elements.
- **Stack** widget for overlapping UI elements.
- **Positioned** widget for positioning UI elements within a **Stack**.
- **Text** widget for displaying text in various styles and formats.

## License

This digital banking app is released under the MIT License. See the LICENSE file for more information.

## Credits

The design for this digital banking app was created by [Farrel Putra](https://dribbble.com/farrelputra) and is used with permission.
