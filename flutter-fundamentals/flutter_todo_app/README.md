# Todo App

This is a simple to-do app developed using Flutter. The purpose of this app is to introduce the basic Flutter widgets and demonstrate how to build a real-world application using them.

## Features

The to-do app has the following features:

- A progress bar at the top of the screen, indicating the user's progress in completing their to-do list.
- A list of to-do items, displayed below the progress bar. Each item displays the task name, due date, and completion status.
- A floating action button in the bottom right corner of the screen, which allows the user to add a new to-do item to their list.

## Technologies Used

This to-do app was developed using Flutter, an open-source framework for building high-performance, high-fidelity mobile applications for iOS and Android, using a single codebase.

The app uses basic Flutter widgets to create the user interface, including:

- **Scaffold** widget for setting up the app's basic layout.
- **Column** and Row widgets for organizing UI elements vertically and horizontally.
- **Container** widget for creating reusable UI components with styling.
- **Text** widget for displaying text in various styles and formats.
- **ListView** widget for displaying a scrollable list of items.
- **CircularProgressIndicator** widget for displaying a circular progress bar.
- **FloatingActionButton** widget for displaying a button that floats above other UI elements.

These widgets are used in combination to create the various components of the to-do app, such as the progress bar, list of to-do items, and floating action button.

## License

This to-do app is released under the MIT License. See the LICENSE file for more information.

## Credits

This to-do app was developed by S.D.C.
